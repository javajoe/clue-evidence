package clue.gui.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;

public abstract class MapDataTableModel implements TableModel {
  private static final Logger logger = Logger.getLogger(PlayerTableModel.class);
  private final List<TableModelListener> listeners;
  protected final Map<Integer, Map<Integer, Object>> data;

  public MapDataTableModel() {
    listeners = new LinkedList<TableModelListener>();
    data = new HashMap<Integer, Map<Integer, Object>>();
  }

  @Override
  public void addTableModelListener(TableModelListener l) {
    listeners.add(l);
  }

  @Override
  public void removeTableModelListener(TableModelListener l) {
    listeners.remove(l);
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    return getColumn(getRow(rowIndex), columnIndex);
  }

  @Override
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    if (aValue == null) {
      if (logger.isDebugEnabled()) {
        logger.debug(String.format("Not storing null at [%s,%s]", rowIndex, columnIndex));
      }
      return;
    }
    Map<Integer, Object> row = getRow(rowIndex);
    Object value = getColumn(row, columnIndex);
    if (value != null) {
      logger.warn(String.format("Replacing at [%s,%s] %s with %s", rowIndex, columnIndex, value, aValue));
    }
    row.put(Integer.valueOf(columnIndex), aValue);
    fireTableChanged(new TableModelEvent(this, rowIndex, rowIndex, columnIndex));
  }

  private Object getColumn(Map<Integer, Object> row, int columnIndex) {
    Object value = row.get(Integer.valueOf(columnIndex));
    return value;
  }

  private Map<Integer, Object> getRow(int rowIndex) {
    Map<Integer, Object> row = data.get(Integer.valueOf(rowIndex));
    if (row == null) {
      row = new HashMap<Integer, Object>();
      data.put(Integer.valueOf(rowIndex), row);
    }
    return row;
  }

  private void fireTableChanged(TableModelEvent e) {
    for (TableModelListener listener : listeners) {
      listener.tableChanged(e);
    }
  }

}
