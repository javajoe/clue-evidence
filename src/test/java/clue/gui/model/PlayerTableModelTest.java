package clue.gui.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import clue.model.Suspect;

public class PlayerTableModelTest {

  private PlayerTableModel tableModel;

  @Before
  public void setUp() {
    tableModel = new PlayerTableModel();
  }

  @Test
  public void testGetValueAt() {
    assertEquals("We should have as many rows as suspects", tableModel.getRowCount(), Suspect.values().length);
    assertEquals("We only have some columns", tableModel.getColumnCount(), 2);
    String expected = "expected";
    for (int row = 0; row < tableModel.getRowCount(); row++) {
      for (int col = 0; col < tableModel.getColumnCount(); col++) {
        expected = String.format("expected [%s,%s]", row, col);
        tableModel.setValueAt(expected, row, col);
        assertEquals(tableModel.getValueAt(row, col), expected);
      }
    }
  }
}
