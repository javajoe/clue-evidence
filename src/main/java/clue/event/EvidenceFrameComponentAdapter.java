package clue.event;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import clue.gui.EvidenceFrame;
import clue.util.Prefs;

public class EvidenceFrameComponentAdapter implements ComponentListener {

  private final EvidenceFrame frame;

  public EvidenceFrameComponentAdapter(EvidenceFrame frame) {
    this.frame = frame;

    frame.setSize(Prefs.userNode(this.frame.getClass()).getInt("width", 1024), Prefs.userNode(this.frame.getClass()).getInt("height", 600));
    frame.setLocation(Prefs.userNode(this.frame.getClass()).getInt("x", 10), Prefs.userNode(this.frame.getClass()).getInt("y", 100));
  }

  @Override
  public void componentMoved(ComponentEvent event) {
    Prefs.userNode(this.frame.getClass()).putInt("x", event.getComponent().getX());
    Prefs.userNode(this.frame.getClass()).putInt("y", event.getComponent().getY());

  }

  @Override
  public void componentResized(ComponentEvent event) {
    Prefs.userNode(this.frame.getClass()).putInt("width", event.getComponent().getWidth());
    Prefs.userNode(this.frame.getClass()).putInt("height", event.getComponent().getHeight());
  }

  @Override
  public void componentHidden(ComponentEvent event) {
  }

  @Override
  public void componentShown(ComponentEvent event) {
  }
}
