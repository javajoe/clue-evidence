package clue.event;

import java.util.List;

import clue.engine.event.EngineEventAdapter;
import clue.gui.PlayerPanel;
import clue.gui.RumorPanel;
import clue.model.Player;
import clue.model.Rumor;

public class PlayerRumorListener {

  private final PlayerPanel playerPanel;
  private final RumorPanel rumorPanel;

  public PlayerRumorListener(PlayerPanel playerPanel, RumorPanel rumorPanel) {
    this.playerPanel = playerPanel;
    this.rumorPanel = rumorPanel;
    initListeners();
  }

  private void initListeners() {
    playerPanel.addEngineEventListener(new EngineEventAdapter() {

      @Override
      public void players(List<Player> players) {
        rumorPanel.setPlayers(players);
      }

      @Override
      public void rumor(Rumor rumor) {
        rumorPanel.addRumor(rumor);
      }
    });
  }

}
