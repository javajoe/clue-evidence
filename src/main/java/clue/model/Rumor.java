package clue.model;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.Property;

public class Rumor {

  private final Player player;
  private final Suspect suspect;
  private final Weapon weapon;
  private final Room room;

  public Rumor(Player player, Suspect suspect, Weapon weapon, Room room) {
    this.player = player;
    this.suspect = suspect;
    this.weapon = weapon;
    this.room = room;
  }

  @Property
  public Player getPlayer() {
    return player;
  }

  @Property
  public Suspect getSuspect() {
    return suspect;
  }

  @Property
  public Weapon getWeapon() {
    return weapon;
  }

  @Property
  public Room getRoom() {
    return room;
  }

  @Override
  public String toString() {
    return Pojomatic.toString(this);
  }
}
