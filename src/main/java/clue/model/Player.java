package clue.model;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.Property;

public class Player {

  private final Suspect suspect;
  private String name;

  public Player(Suspect suspect) {
    this(suspect, String.format("%s name", suspect.name()));
  }

  public Player(Suspect suspect, String name) {
    this.suspect = suspect;
    this.name = name;
  }

  @Property
  public Suspect getSuspect() {
    return this.suspect;
  }

  @Property
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return Pojomatic.toString(this);
  }

  @Override
  public int hashCode() {
    return Pojomatic.hashCode(this);
  }

  @Override
  public boolean equals(Object obj) {
    return Pojomatic.equals(this, obj);
  }
}
