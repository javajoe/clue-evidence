package clue.gui.model;

import clue.model.Rumor;

public class RumorTableModel extends MapDataTableModel {
  @Override
  public int getRowCount() {
    return this.data.size();
  }

  @Override
  public int getColumnCount() {
    return 5;
  }

  @Override
  public String getColumnName(int columnIndex) {
    switch (columnIndex) {
    case 0:
      return "Who Rumored";
    case 1:
      return "Suspect";
    case 2:
      return "Weapon";
    case 3:
      return "Room";
    case 4:
      return "Who Showed";
    }
    return String.format("?%s", columnIndex);
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    return String.class;
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    if (columnIndex == 4) {
      return true;
    }
    return false;
  }

  public void addRumor(Rumor rumor) {
    if (rumor == null) {
      throw new IllegalArgumentException("Please provide rumor not null");
    }
    int row = getRowCount();
    setValueAt(rumor.getPlayer(), row, 0);
    setValueAt(rumor.getSuspect(), row, 1);
    setValueAt(rumor.getWeapon(), row, 2);
    setValueAt(rumor.getRoom(), row, 3);
  }
}
