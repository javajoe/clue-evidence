package clue.gui.model;

import org.apache.commons.lang.StringUtils;

import clue.model.Player;
import clue.model.Suspect;

public class PlayerTableModel extends MapDataTableModel {

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    return String.class;
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public String getColumnName(int columnIndex) {
    switch (columnIndex) {
    case 0:
      return "Suspect";
    case 1:
      return "Player's Name";
    }
    return String.format("?%s", columnIndex);
  }

  @Override
  public int getRowCount() {
    return 6;
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    switch (columnIndex) {
    case 0:
      return false;
    case 1:
      return true;
    }
    throw new RuntimeException(String.format("isCellEditable(%s,%s)??", rowIndex, columnIndex));
  }

  public Player getPlayerAt(int row) {
    String suspectName = (String) getValueAt(row, 0);
    String playerName = (String) getValueAt(row, 1);
    if (!StringUtils.isBlank(suspectName) && !StringUtils.isBlank(playerName)) {
      return new Player(Suspect.valueOf(suspectName), playerName);
    }
    return null;
  }

  public Suspect getSuspectAt(int row) {
    String s = (String) getValueAt(row, 0);
    return Suspect.valueOf(s);
  }
}
