package clue.gui;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import clue.model.Player;

public class PlayerCellRenderer extends DefaultTableCellRenderer {
  private static final long serialVersionUID = 20110218L;

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    if (value != null) {
      if (value instanceof String) {
        this.setText(value.toString());
      } else if (value instanceof Player) {
        Player player = (Player) value;
        this.setText(player.getName());
      } else {
        throw new RuntimeException(String.format("We expected a %s, not a %s", Player.class.getCanonicalName(), value.getClass().getCanonicalName()));
      }
    }
    return this;
  }
}
