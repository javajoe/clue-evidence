package clue.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import clue.event.EvidenceFrameComponentAdapter;
import clue.event.PlayerRumorListener;

public class EvidenceFrame extends JFrame {

  private static final long serialVersionUID = 20110216L;
  private PlayerPanel playerPanel;
  private RumorPanel rumorPanel;
  private ControlPanel controlPanel;

  public EvidenceFrame() {
    super("Clue Evidence");
    rumorPanel = new RumorPanel();
    playerPanel = new PlayerPanel(this);
    controlPanel = new ControlPanel();
    initGui();
    initListeners();
  }

  private void initGui() {
    add(controlPanel, BorderLayout.NORTH);
    add(playerPanel, BorderLayout.WEST);
    add(rumorPanel, BorderLayout.EAST);
  }

  private void initListeners() {
    this.addComponentListener(new EvidenceFrameComponentAdapter(this));
    rumorPanel.setPlayers(playerPanel.getPlayers());
    new PlayerRumorListener(playerPanel, rumorPanel);
  }
}
