package clue.gui;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.apache.log4j.Logger;

import clue.gui.model.RumorTableModel;
import clue.model.Player;
import clue.model.Rumor;

public class RumorPanel extends JPanel {
  private static final long serialVersionUID = 20110217L;

  private static final Logger logger = Logger.getLogger(RumorPanel.class);
  private final JTable table;

  private final RumorTableModel rumorTableModel;
  private final JComboBox players;

  public RumorPanel() {
    players = new JComboBox(new Object[] { "" });
    rumorTableModel = new RumorTableModel();
    table = new JTable(rumorTableModel);
    initGui();
    initListeners();
  }

  private void initGui() {
    table.getColumnModel().getColumn(0).setCellRenderer(new PlayerCellRenderer());
    table.getColumnModel().getColumn(4).setCellRenderer(new PlayerCellRenderer());
    table.getColumnModel().getColumn(4).setCellEditor(new DefaultCellEditor(players));
    add(new JScrollPane(table));
  }

  private void initListeners() {

  }

  public void addRumor(Rumor rumor) {
    rumorTableModel.addRumor(rumor);
    if (logger.isDebugEnabled()) {
      logger.debug(String.format("Rumor"));
    }
    table.revalidate();
  }

  public void setPlayers(List<Player> list) {
    List<String> aList = new LinkedList<String>();
    aList.add("");
    for (Player player : list) {
      if (player.getName() != null) {
        aList.add(player.getName());
      }
    }
    Collections.sort(aList, new Comparator<String>() {

      @Override
      public int compare(String lhs, String rhs) {
        return lhs.compareTo(rhs);
      }
    });
    players.setModel(new DefaultComboBoxModel(aList.toArray(new String[aList.size()])));
  }
}
