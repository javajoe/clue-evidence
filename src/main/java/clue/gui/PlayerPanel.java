package clue.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.apache.commons.lang.StringUtils;

import clue.engine.event.EngineEventAdapter;
import clue.engine.event.EngineEventListener;
import clue.gui.model.PlayerTableModel;
import clue.model.Player;
import clue.model.Room;
import clue.model.Rumor;
import clue.model.Suspect;
import clue.model.Weapon;
import clue.util.Prefs;

public class PlayerPanel extends JPanel {

  private static final long serialVersionUID = 20110216L;
  private final List<EngineEventListener> listeners;
  private final JTable table;
  private final JComboBox room;
  private final JComboBox suspect;
  private final JComboBox weapon;
  private final JButton rumor;
  private final PlayerTableModel playerTableModel;
  private final Component parent;

  public PlayerPanel(Component parent) {
    this.parent = parent;
    listeners = new LinkedList<EngineEventListener>();
    room = new JComboBox(new DefaultComboBoxModel(Room.values()));
    suspect = new JComboBox(new DefaultComboBoxModel(Suspect.values()));
    weapon = new JComboBox(new DefaultComboBoxModel(Weapon.values()));
    playerTableModel = new PlayerTableModel();
    table = new JTable(playerTableModel);
    rumor = new JButton("Rumor");
    initGui();
    initListeners();
  }

  private void initGui() {
    Box vBox = Box.createVerticalBox();
    vBox.add(new JLabel("Current Rumor"));
    Box box = Box.createHorizontalBox();
    box.add(addRow("Suspect", suspect));
    box.add(addRow("Weapon", weapon));
    box.add(addRow("Room", room));
    vBox.add(box);
    vBox.add(rumor);
    vBox.add(new JScrollPane(table));
    int rowIndex = 0;
    for (Suspect suspect : Suspect.values()) {
      table.setValueAt(suspect.name(), rowIndex, 0);
      table.setValueAt(Prefs.userNode(this.getClass()).get(suspect.name(), ""), rowIndex, 1);
      rowIndex++;
    }
    add(vBox);
  }

  private void initListeners() {
    rumor.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        Player selectedPlayer = getSelectedPlayer();
        if (selectedPlayer != null) {
          makeRumor(new Rumor(selectedPlayer, (Suspect) suspect.getSelectedItem(), (Weapon) weapon.getSelectedItem(), (Room) room.getSelectedItem()));
        } else {
          JOptionPane.showMessageDialog(parent, "Please select a player before making a rumor");
        }
      }
    });
    table.getModel().addTableModelListener(new TableModelListener() {

      @Override
      public void tableChanged(TableModelEvent e) {
        PlayerTableModel tableModel = (PlayerTableModel) e.getSource();
        Player player = tableModel.getPlayerAt(e.getFirstRow());
        if (player != null) {
          Prefs.userNode(PlayerPanel.this.getClass()).put(player.getSuspect().name(), player.getName());
        } else {
          Prefs.userNode(PlayerPanel.this.getClass()).remove(tableModel.getSuspectAt(e.getFirstRow()).name());
        }
        SwingUtilities.invokeLater(new Runnable() {

          @Override
          public void run() {
            for (EngineEventListener listener : listeners) {
              listener.players(getPlayers());
            }
          }
        });
      }
    });
    addEngineEventListener(new EngineEventAdapter() {
    });
  }

  public void addEngineEventListener(EngineEventListener engineEventListener) {
    listeners.add(engineEventListener);
  }

  public void removeEngineEventListener(EngineEventListener engineEventListener) {
    listeners.remove(engineEventListener);
  }

  public List<Player> getPlayers() {
    final List<Player> players = new LinkedList<Player>();
    for (int row = 0; row < playerTableModel.getRowCount(); row++) {
      Player player = playerTableModel.getPlayerAt(row);
      if (player != null) {
        players.add(player);
      }
    }
    return players;
  }

  protected void makeRumor(Rumor rumor) {
    for (EngineEventListener listener : listeners) {
      listener.rumor(rumor);
    }
  }

  private Box addRow(String label, JComboBox comboBox) {
    Box box = Box.createVerticalBox();
    box.add(new JLabel(label));
    box.add(comboBox);
    return box;
  }

  private Player getSelectedPlayer() {
    return playerTableModel.getPlayerAt(table.getSelectedRow());
  }
}
