package clue.gui;

import javax.swing.JPanel;

public class ControlPanel extends JPanel {

  private static final long serialVersionUID = 20110216L;

  public ControlPanel() {
    initGui();
    initListeners();
  }

  private void initGui() {
  }

  private void initListeners() {
  }
}
