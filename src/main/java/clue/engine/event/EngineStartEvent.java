package clue.engine.event;

import java.util.LinkedList;
import java.util.List;

import clue.model.Player;

public class EngineStartEvent {

  private final List<Player> players;

  public EngineStartEvent(List<Player> players) {
    this.players = new LinkedList<Player>(players);
  }

  public List<Player> getPlayers() {
    return players;
  }

}
