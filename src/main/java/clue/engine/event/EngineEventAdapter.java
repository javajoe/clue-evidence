package clue.engine.event;

import java.util.List;

import clue.model.Player;
import clue.model.Rumor;

public class EngineEventAdapter implements EngineEventListener {
  @Override
  public void rumor(Rumor rumor) {
  }

  @Override
  public void players(List<Player> players) {
  }
}
