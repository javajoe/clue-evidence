package clue.engine.event;

import java.util.List;

import clue.model.Player;
import clue.model.Rumor;


public interface EngineEventListener {
  void rumor(Rumor rumor);
  void players(List<Player> players);
}
