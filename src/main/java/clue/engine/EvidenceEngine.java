package clue.engine;

import java.util.LinkedList;
import java.util.List;

import clue.engine.event.EngineEventListener;
import clue.model.Player;
import clue.model.Suspect;

public final class EvidenceEngine {

  private static final List<EngineEventListener> listeners;
  private static final List<Player> players;
  private static final Object lock;
  static {
    lock = new Object();
    listeners = new LinkedList<EngineEventListener>();
    players = new LinkedList<Player>();
  }

  private EvidenceEngine() {
    // singlton
  }

  public static void playerCountChanged(int playerCount) {
    players.clear();
    for (int i = 0; i < playerCount; i++) {
      players.add(new Player(Suspect.values()[i]));
    }
  }

  public static void addEngineEventListener(EngineEventListener listener) {
    synchronized (lock) {
      listeners.add(listener);
    }
  }

  public static void removeEngineEventListener(EngineEventListener listener) {
    synchronized (lock) {
      listeners.remove(listener);
    }
  }
}
